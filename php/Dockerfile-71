FROM php:7.1-fpm

MAINTAINER Sompong Himalee <sompong@edispro.com>

ADD ./codeigniter.ini /usr/local/etc/php/conf.d
ADD ./vh-codeigniter.pool.conf /usr/local/etc/php-fpm.d/

RUN apt-get update && apt-get install -y \
  libpq-dev \
  libmemcached-dev \
  curl \
  mysql-client \
  postgresql-client \
  libpng12-dev \
  libfreetype6-dev \
  libssl-dev \
  libmcrypt-dev \
  --no-install-recommends && \
  rm -r /var/lib/apt/lists/*

# Install mongodb driver
RUN pecl install mongodb

# configure gd library
RUN docker-php-ext-configure gd \
  --enable-gd-native-ttf \
  --with-freetype-dir=/usr/include/freetype2

# Install extensions using the helper script provided by the base image
RUN docker-php-ext-install \
  pdo_mysql \
  pdo_pgsql \
  mysqli \
  gd \
  mcrypt
#####################################
# PHP REDIS EXTENSION FOR PHP 7.1
#####################################
RUN     printf "\n" | pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis;
#####################################
# ImageMagick:
#####################################
USER root

RUN   apt-get update -y && \
    apt-get install -y libmagickwand-dev imagemagick && \ 
    pecl install imagick && \
    docker-php-ext-enable imagick;	
# Install xdebug
RUN pecl install xdebug && \
  docker-php-ext-enable xdebug

RUN usermod -u 1000 www-data

WORKDIR /var/www/codeigniter

CMD ["php-fpm"]

EXPOSE 9000
